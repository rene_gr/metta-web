(defproject metta.at "0.1.0"
  :description "Be kind."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.5.3"
  
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.170"]
                 [org.clojure/core.async "0.2.374"
                  :exclusions [org.clojure/tools.reader]]

                 ;; Webserver
                 [http-kit "2.1.18"]
                 [com.taoensso/sente "1.7.0"]
                 [compojure "1.4.0"]
                 [javax.servlet/servlet-api "2.5"]
                 [ring/ring-defaults        "0.1.5"]        ;; includes anti-forgery

                 ;; Reagent
                 [reagent "0.6.0-alpha"]

                 ;; Environment variables
                 [environ "1.0.2"]]
  
  :plugins [[lein-figwheel "0.5.0-6"]
            [lein-cljsbuild "1.1.2" :exclusions [[org.clojure/clojure]]]]

  :source-paths ["src/clj"]
  :main ^:skip-aot metta.at.core

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :uberjar-name "metta-at-web.jar"
  :profiles     {:uberjar      {:source-paths ^:replace ["src/clj"]
                                :hooks        [leiningen.cljsbuild]
                                :omit-source  true
                                :aot          :all
                                :main         metta.at.core
                                :cljsbuild    {:builds {:dev {:source-paths ^:replace ["src/cljs"]
                                                              :compiler     {:optimizations :advanced
                                                                             :pretty-print false}}}}}}

  :cljsbuild {:builds {:dev {:source-paths ["src/cljs"]

                             ;; If no code is to be run, set :figwheel true for continued automagical reloading
                             :figwheel     {:on-jsload "metta.at.core/on-js-reload"}

                             :compiler     {:main                 metta.at.core
                                            :asset-path           "js/compiled/out"
                                            :output-to            "resources/public/js/compiled/app.js"
                                            :output-dir           "resources/public/js/compiled/out"
                                            :source-map-timestamp true}}
                       ;; This next build is an compressed minified build for
                       ;; production. You can build this with:
                       ;; lein cljsbuild once min
                       :min {:source-paths ["src/cljs"]
                             :compiler     {:output-to     "resources/public/js/compiled/app.js"
                                            :main          metta.at.core
                                            :optimizations :advanced
                                            :pretty-print  false}}}}

  :figwheel {;; :http-server-root "public" ;; default and assumes "resources"
             ;; :server-port 3449 ;; default
             ;; :server-ip "127.0.0.1"

             :css-dirs ["resources/public/css"] ;; watch and update CSS

             ;; Start an nREPL server into the running figwheel process
             ;; :nrepl-port 7888

             ;; Server Ring Handler (optional)
             ;; if you want to embed a ring handler into the figwheel http-kit
             ;; server, this is for simple ring servers, if this
             ;; doesn't work for you just run your own server :)
             ;; :ring-handler hello_world.server/handler

             ;; To be able to open files in your editor from the heads up display
             ;; you will need to put a script on your path.
             ;; that script will have to take a file path and a line number
             ;; ie. in  ~/bin/myfile-opener
             ;; #! /bin/sh
             ;; emacsclient -n +$2 $1
             ;;
             ;; :open-file-command "myfile-opener"

             ;; if you want to disable the REPL
             ;; :repl false

             ;; to configure a different figwheel logfile path
             ;; :server-logfile "tmp/logs/figwheel-logfile.log"
             })
