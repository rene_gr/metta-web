(ns metta.at.core
  (:require [compojure.route :refer [resources not-found]]
            [compojure.handler :refer [site]] ; form, query params decode; cookie; session, etc
            [compojure.core :refer [defroutes GET POST DELETE ANY context]]
            [ring.util.response :as resp]
            [ring.middleware.defaults]
            [org.httpkit.server :refer :all]

            [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.http-kit      :refer (sente-web-server-adapter)]

            [environ.core :refer [env]])
  (:gen-class))

;;;; Help functions

(def message-cache (atom []))

(defn unix-time-real
  "Taken from
  System/currentTimeMillis."
  []
  (/ (System/currentTimeMillis) 1000))

(defn cleanup-cache [cache]
  (let [c1 (drop (max 0 (- (count cache) 10)) cache)        ;; keep only the last 10
        now-2h (- (unix-time-real)
                  (* 60 60 2))
        c2 (filter #(> (:time %) now-2h) c1)]               ;; filter out, that are older than 2h
    c2))

;;;; SENTE

(let [{:keys [ch-recv send-fn ajax-post-fn ajax-get-or-ws-handshake-fn
              connected-uids]}
      (sente/make-channel-socket! sente-web-server-adapter {})]
  (def ring-ajax-post                ajax-post-fn)
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
  (def ch-chsk                       ch-recv) ; ChannelSocket's receive channel
  (def chsk-send!                    send-fn) ; ChannelSocket's send API fn
  (def connected-uids                connected-uids) ; Watchable, read-only atom
  )

(defmulti event-msg-handler
          "Multimethod to handle Sente `event-msg`s"
          :id ; Dispatch on event-id
          )

(defmethod event-msg-handler
  :default ; Default/fallback case (no other matching handler)
  [{:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
  (let [session (:session ring-req)
        uid     (:uid     session)]
    (println (str "Unhandled event: " event))
    (when ?reply-fn
      (?reply-fn {:umatched-event-as-echoed-from-from-server event}))))

(defmethod event-msg-handler :chsk/ws-ping [_] identity)

(defmethod event-msg-handler :metta/new-connection [event]
  (println (:uid event))
  #_(println (str "New connection from " (:uid event)))
  (chsk-send! (:uid event) [:metta/all-kindwords {:all @message-cache}]))

(defmethod event-msg-handler :metta/post-kindwords [{:keys [event]}]
  (let [new-message {:message (-> event last :message)
                     :time    (unix-time-real)}
        new-messages (cleanup-cache (conj (vec @message-cache) new-message))]
    (reset! message-cache new-messages)
    (doseq [id (:any @connected-uids)]
      (chsk-send! id [:metta/all-kindwords {:all new-messages}])
      (chsk-send! id [:metta/add-kindwords new-message]))))

(defonce sente-router (atom nil))
(defn  stop-router! [] (when-let [stop-f @sente-router] (stop-f)))
(defn start-router! []
  (stop-router!)
  (reset! sente-router
          (sente/start-chsk-router!
            ch-chsk event-msg-handler)))


;;;; Plain HTTP

(defroutes all-routes
           (GET "/" [] (resp/redirect "index.html"))
           (GET  "/chsk" req (ring-ajax-get-or-ws-handshake req))
           (POST "/chsk" req (ring-ajax-post                req))
           (resources "/") ;; static file url prefix /static, in `public` folder
           (not-found "<p>Page not found.</p>"))

(def app
  (let [ring-defaults-config
        (assoc-in ring.middleware.defaults/site-defaults
                  [:security :anti-forgery]
                  {:read-token (fn [req] (-> req :params :csrf-token))})]

    ;; NB: Sente requires the Ring `wrap-params` + `wrap-keyword-params`
    ;; middleware to work. These are included with
    ;; `ring.middleware.defaults/wrap-defaults` - but you'll need to ensure
    ;; that they're included yourself if you're not using `wrap-defaults`.
    (-> (ring.middleware.defaults/wrap-defaults
          all-routes ring-defaults-config)
        ring.middleware.keyword-params/wrap-keyword-params
        ring.middleware.params/wrap-params)))

(defn -main [& [port]]
  (let [port (Integer. (or port (env :port) 8080))]
    (println "Started HTTP Server " port " ... ")
    (start-router!)
    (run-server app {:port port})))
