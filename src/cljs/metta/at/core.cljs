(ns metta.at.core
  (:require-macros
    [cljs.core.async.macros :as asyncm :refer (go go-loop)])
  (:require [reagent.core :as r]
            [cljs.core.async :as async :refer (<! >! put! chan)]
            [taoensso.sente  :as sente :refer (cb-success?)]))

;; Init

(enable-console-print!)

(defonce app-state (r/atom {:messages []}))

(defonce sente-communication (let [{:keys [chsk ch-recv send-fn state]}
                                     (sente/make-channel-socket! "/chsk" ; Note the same path as before
                                                                 {:type :auto ; e/o #{:auto :ajax :ws}
                                                                  })]
                                 (def chsk       chsk)
                                 (def ch-chsk    ch-recv) ; ChannelSocket's receive channel
                                 (def chsk-send! send-fn) ; ChannelSocket's send API fn
                                 (def chsk-state state)   ; Watchable, read-only atom
                                 ))


;;;; Sente event handlers

(defmulti domain-message-handler
          "Multimethod to handle internal messages"
          first)
(defmethod domain-message-handler :metta/add-kindwords [[_ message]]
  identity
  #_(swap! app-state #(assoc % :messages
                             (conj (:messages %) message))))
(defmethod domain-message-handler :metta/all-kindwords [[_ message]]
  (swap! app-state #(assoc % :messages (:all message))))



(defmulti event-msg-handler
          "Multimethod to handle Sente `event-msg`s"
          :id                                               ; Dispatch on event-id
          )
(defmethod event-msg-handler :chsk/state [_] identity)
(defmethod event-msg-handler :chsk/handshake [_] (chsk-send! [:metta/new-connection {}])) ;; Request initial data
(defmethod event-msg-handler :chsk/recv [event]
  (domain-message-handler (-> event :event last)))

;;;; Sente event router (our `event-msg-handler` loop)

(defonce sente-router (atom nil))
(defn  stop-router! [] (when-let [stop-f @sente-router] (stop-f)))
(defn  start-router! []
  (stop-router!)
  (reset! sente-router
          (sente/start-chsk-router!
            ch-chsk event-msg-handler)))


;;;; UI

(defn input-field []
  [:div.row
   [:form.col.s12 {:on-submit (fn [evnt]
                                (.preventDefault evnt)
                                (let [elm (js/document.getElementById "kindwords")
                                      msg (.-value elm)]
                                  (when (> (count msg) 1)
                                    (chsk-send! [:metta/post-kindwords {:message msg}]))
                                  (aset elm "value" "")))}
    [:div.row
     [:div.input-field.col.s12
      [:i.material-icons.prefix "chat_bubble"]
      [:input#icon_prefix.validate {:type "text" :autoComplete "off" :id "kindwords"}]
      [:label {:for "icon_prefix"} "Hier kannst du ein paar liebevolle Worte hinterlassen."]]]]])

(defn message-box []
  [:div.row
   [:div.col.s12.m4
    [:div.card
     [:div.card-content
      [:span.card-title "METTA-SUTTA"]
      [:br]
      "Wem klar geworden, dass der Frieden des Geistes\ndas Ziel seines Lebens ist,
      der bemühe sich um folgende Gesinnung:\nEr sei stark / aufrecht und gewissenhaft /
      freundlich / sanft und ohne Stolz.\nGenügsam sei er / leicht befriedigt /
      nicht viel geschäftig und bedürfnislos.\nDie Sinne still / klar der Verstand /
      nicht dreist / nicht gierig sei sein Verhalten.\nAuch nicht im Kleinsten soll er sich vergehen /
      wofür ihn Verständige tadeln könnten.\nMögen alle Wesen glücklich sein und Frieden finden.
      Was es auch an lebenden Wesen gibt:\nob stark oder schwach / ob groß oder klein /
      ob sichtbar oder unsichtbar / fern oder nah /
      ob geboren oder einer Geburt zustrebend -\nmögen sie alle glücklich sein.
      Niemand betrüge oder verachte einen anderen.\nAus Ärger oder Übelwollen wünsche man keinem
      irgendwelches Unglück.\nWie eine Mutter mit ihrem Leben\nihr einzig Kind beschützt und behütet /
      so möge man für alle Wesen und die ganze Welt\nein unbegrenzt gütiges Gemüt erwecken:
      ohne Hass / ohne Feindschaft / ohne Beschränkung\nnach oben / nach unten und nach allen Seiten.
      Im Gehen oder Stehen / im Sitzen oder Liegen /\nentfalte man eifrig diese Gesinnung:
      Dies nennt man Weilen im Heiligen.\nWer sich nicht an Ansichten verliert /
      Tugend und Einsicht gewinnt /\ndem Sinnengenuss nicht verhaftet ist –
      für den gibt es keine Geburt mehr. "
      [:a {:href "http://www.buddhistische-gesellschaft-berlin.de/downloads/mettavimalo06.pdf"} "Quelle"]]]]
   [:div.col.s12.m8
    [:div.card
     [:div.card-content
      [:span.card-title "Freundliche Worte"]
      [:p
       "Wenn du möchtest, kannst du hier ein paar freundliche Worte hinterlassen."
       [:br]
       "Deine Worte werden für einen kurzen Zeitraum für Besucher von metta.at zu sehen sein."]

      [:ul
       (for [{:keys [time message]} (:messages @app-state)]
         ^{:key time} [:li message])]]
     [:div.card-action
      [input-field]
      #_[:a {:href "#"} "This is a link"]
      #_[:a {:href "#"} "This is a link"]]]]])

(defn app []
  [:div
   #_[:nav.white
    {:role "navigation"}
    [:div.nav-wrapper.container
     [input-field]
     #_[:a#logo-container.brand-logo {:href "#"} "Logo"]
     #_[:ul.right.hide-on-med-and-down
        [:li [:a {:href "#"} "Navbar Link"]]]
     #_[:ul#nav-mobile.side-nav [:li [:a {:href "#"} "Navbar Link"]]]
     #_[:a.button-collapse
        {:data-activates "nav-mobile", :href "#"}
        [:i.material-icons "menu"]]]]
   [:div#index-banner.parallax-container
    [:div.section.no-pad-bot
     [:div.container
      [:br]
      [:br]
      #_[:h1.header.center.teal-text.text-lighten-2 "Parallax Template"]
      #_[:div.row.center
       [:h5.header.col.s12.light
        "A modern responsive front-end framework based on Material Design"]]
      #_[:div.row.center
       [:a#download-button.btn-large.waves-effect.waves-light.teal.lighten-1
        {:href "http://materializecss.com/getting-started.html"}
        "Get Started"]]
      [:div.row.center
       [message-box]]
      [:br]
      [:br]]]
    [:div.parallax
     [:img
      {:alt "https://de.wikipedia.org/wiki/Metta#/media/File:Buddha_with_the_Elephant_Nalagiri.jpg", :src "images/Buddha_with_the_Elephant_Nalagiri.jpg"}]]]

   [:footer.page-footer.teal
    (comment
      [:div.container
       [:div.row
        [:div.col.l6.s12
         [:h5.white-text "Company Bio"]
         [:p.grey-text.text-lighten-4
          "We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated."]]
        [:div.col.l3.s12
         [:h5.white-text "Settings"]
         [:ul
          [:li [:a.white-text {:href "#!"} "Link 1"]]
          [:li [:a.white-text {:href "#!"} "Link 2"]]
          [:li [:a.white-text {:href "#!"} "Link 3"]]
          [:li [:a.white-text {:href "#!"} "Link 4"]]]]
        [:div.col.l3.s12
         [:h5.white-text "Connect"]
         [:ul
          [:li [:a.white-text {:href "#!"} "Link 1"]]
          [:li [:a.white-text {:href "#!"} "Link 2"]]
          [:li [:a.white-text {:href "#!"} "Link 3"]]
          [:li [:a.white-text {:href "#!"} "Link 4"]]]]]])
    [:div.footer-copyright
     [:div.container
      "metta.at 2016 "
      [:span.brown-text.text-lighten-3
       [:a {:href "https://de.wikipedia.org/wiki/Metta#/media/File:Buddha_with_the_Elephant_Nalagiri.jpg"}
        "Bildquelle"]]]]]])


(defn ^:export run []
  (r/render [app]
            (js/document.getElementById "app"))
  (start-router!))

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  (run)
  (js/document.updateJquery)
)
